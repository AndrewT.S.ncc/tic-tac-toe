package com.example.tictactoe;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class Welcome extends AppCompatActivity {

    private Intent intent;
    private EditText playerXeditText, playerOeditText;
    private String playerXname, playerOname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        intent = new Intent(this, MainActivity.class);

        playerXeditText = findViewById(R.id.playerXeditText);
        playerOeditText = findViewById(R.id.playerOeditText);
    }

    /**
     * Check to see if both names entered are not empty strings and then starts the game
     *
     * @param v start button that was clicked
     */
    public void startGame(View v) {
        playerXname = playerXeditText.getText().toString();
        playerOname = playerOeditText.getText().toString();

        if (playerXname.equals("") || playerOname.equals("")) {
            Toast.makeText(getApplicationContext(), "Can not leave a name field blank", Toast.LENGTH_LONG).show();
            return;
        }
        intent.putExtra("playerXname", playerXname);
        intent.putExtra("playerOname", playerOname);

        startActivity(intent);
        finish();
    }

    /**
     * Skips entering names if the players do not desire
     *
     * @param v - the skip button that was clicked
     */
    public void skip(View v) {
        startActivity(intent);
        finish();
    }
}
