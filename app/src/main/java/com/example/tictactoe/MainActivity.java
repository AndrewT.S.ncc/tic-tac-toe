package com.example.tictactoe;
/**
 * Copyright (C) 2019 The CSC 240 Instructors & Students.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    // Tic-Tac-Toe gameplay buttons are labeled in an (y,x) fashion starting in the NW corner, zero-based, and the y-axis counting down positive
    //  The center of the board is button (1,1) i.e. typical 2D matrix visualization.

    private Intent intent, welcome;
    private int colorPrimary; // default UI color
    private int colorSecondary;  // default UI color
    private String playerXname, playerXnamePlural, playerOname, playerOnamePlural; // player names
    private TextView gameStatus;
    private int xColor; // color of `X'
    private int oColor; // color of `O'
    private TypedValue typedValue = new TypedValue(); // used as middleman to pull colors from the defaultTheme
    private int[][] gameBoard = new int[3][3];  // game board of ints to represent where players are in the game
    private Button[][] btnBoard = new Button[3][3]; // array of buttons on the gameboard
    // both of these arrays of buttons and ints and one-to-one position based, the gameBoard[1][2] corresponds to btnBoard[1][2]
    private int numPlays = 0; // how many plays since the beginning of the game
    private boolean xORo = false;  // Keeps track of who's turn it is; False = X and True = O
    private StringBuilder sb;

    @Override
    /**
     * First method called on apps launch, acts as main()
     * Gets default colors from appTheme
     * Sets default colors for `X' and `O'
     * Assigns UI buttons to the btnBoard and sets a listener to them
     */
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        intent = getIntent(); // used for retrieving player names from prev. activity
        welcome = new Intent(this, Welcome.class); // alows the settings button to go back to playername input

        playerXname = intent.getStringExtra("playerXname");
        playerOname = intent.getStringExtra("playerOname");

        if (playerXname == null)
            playerXname = "X";
        if (playerOname == null)
            playerOname = "O";

        // Handles adding 's or s' to end of name for plural form
        sb = new StringBuilder();
        if (playerXname.charAt(playerXname.length() - 1) == 's' || playerXname.charAt(playerXname.length() - 1) == 'S') {
            sb.append(playerXname);
            sb.append("'");
            playerXnamePlural = sb.toString();
        } else {
            sb.append(playerXname);
            sb.append("'s");
            playerXnamePlural = sb.toString();
        }
        sb = new StringBuilder();
        if (playerOname.charAt(playerOname.length() - 1) == 's' || playerOname.charAt(playerOname.length() - 1) == 'S') {
            sb.append(playerOname);
            sb.append("'");
            playerOnamePlural = sb.toString();
        } else {
            sb.append(playerOname);
            sb.append("'s");
            playerOnamePlural = sb.toString();
        }

        gameStatus = findViewById(R.id.gameStatusText);
        gameStatus.setText(playerXnamePlural + " turn"); // X always goes first


        getTheme().resolveAttribute(R.attr.colorPrimary, typedValue, true);
        colorPrimary = typedValue.data; // Get value of primary app color
        getTheme().resolveAttribute(R.attr.colorSecondary, typedValue, true);
        colorSecondary = typedValue.data; // Get value of secondary app color
        xColor = Color.BLACK; // set default color for `X'
        oColor = Color.RED; // set default color for `O'

        int btnPtr = R.id.btn00; // loop through all the buttons on the game board and assign them to the btnBoard
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                btnBoard[i][j] = findViewById(btnPtr);
                btnBoard[i][j].setOnClickListener(this); // attaches a listener to the buttons
                btnPtr++;
            }
        }
    }

    /**
     * Saves current status of game so it can be restored
     *
     * @param outState - bundle containing savestates
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {

        //putArray can only take a 1-D array so I have to convert my 2-D arrays to 1-D

        int[] gameBoard1R = new int[9];
        char[] btnBoard1R = new char[9];
        boolean[] clickable = new boolean[9];
        for (int i = 0; i < 9; ) {
            for (int j = 0; j < 3; j++) {
                for (int k = 0; k < 3; k++) {
                    gameBoard1R[i] = gameBoard[j][k];
                    if (!btnBoard[j][k].getText().toString().equals("")) // can't store an empty string in a char
                        btnBoard1R[i] = btnBoard[j][k].getText().charAt(0);
                    if (btnBoard[j][k].isClickable())
                        clickable[i] = true;
                    i++;
                }
            }
        }
        outState.putIntArray("gameBoard", gameBoard1R);
        outState.putCharArray("btnBoard", btnBoard1R);
        outState.putBooleanArray("clickable", clickable);

        outState.putString("gameStatus", gameStatus.getText().toString());
        outState.putInt("xColor", xColor);
        outState.putInt("oColor", oColor);
        outState.putInt("numPlays", numPlays);
        outState.putBoolean("xORo", xORo);

        super.onSaveInstanceState(outState);
    }

    /**
     * Restores the state of the game to how it was
     *
     * @param savedInstanceState - bundle containing savestates
     */
    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);


        gameStatus.setText(savedInstanceState.getString("gameStatus"));
        numPlays = savedInstanceState.getInt("numPlays");
        xORo = savedInstanceState.getBoolean("xORo");

        // Must convert 1-D array back into a 3-D one, and do some work along the way
        int[] gameBoard1R = savedInstanceState.getIntArray("gameBoard");
        char[] btnBoard1R = savedInstanceState.getCharArray("btnBoard");
        boolean[] clickable = savedInstanceState.getBooleanArray("clickable");
        xColor = savedInstanceState.getInt("xColor");
        oColor = savedInstanceState.getInt("oColor");
        for (int i = 0; i < 9; ) {
            for (int j = 0; j < 3; j++) {
                for (int k = 0; k < 3; k++) {
                    gameBoard[j][k] = gameBoard1R[i];

                    if (gameBoard[j][k] == 1) // recoloring text as we rebuild
                        btnBoard[j][k].setTextColor(xColor);
                    else if (gameBoard1R[i] == 4)
                        btnBoard[j][k].setTextColor(oColor);

                    btnBoard[j][k].setText(Character.toString(btnBoard1R[i]));
                    btnBoard[j][k].setClickable(clickable[i]);
                    i++;
                }
            }
        }
        if (numPlays >= 5) // easier to check for a winner than save and restore colorFilters on winning tiles.
            checkBoard();
    }

    @Override
    /**
     * The listener for the buttons in the action bar
     */
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) { // settings button does nothing for now
            startActivity(welcome);
        }
        if (id == R.id.action_reset) {// Set "xORo" flag to false so first click will place an `X', then set all buttons to an
            // empty String and make them clickable again.
            xORo = false;
            numPlays = 0;

            for (int i = 0; i < 3; i++) { // resets the board to default states
                for (int j = 0; j < 3; j++) {
                    gameBoard[i][j] = 0;
                    btnBoard[i][j].getBackground().clearColorFilter();
                    btnBoard[i][j].setText(R.string.blank);
                    btnBoard[i][j].setClickable(true);
                }
            }
            gameStatus.setText(playerXnamePlural + " turn");
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    /**
     * Sets up the spinner items in the overflow menu
     */
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        MenuItem xSpinnerItem = menu.findItem(R.id.xColor_spinner);
        MenuItem oSpinnerItem = menu.findItem(R.id.oColor_spinner);
        Spinner xSpinner = (Spinner) xSpinnerItem.getActionView();
        Spinner oSpinner = (Spinner) oSpinnerItem.getActionView();
        xSpinner.setOnItemSelectedListener(this);
        oSpinner.setOnItemSelectedListener(this);
        ArrayAdapter<CharSequence> xAdapter = ArrayAdapter.createFromResource(this, R.array.colors_array, android.R.layout.simple_list_item_1);
        xAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ArrayAdapter<CharSequence> oAdapter = ArrayAdapter.createFromResource(this, R.array.colors_array, android.R.layout.simple_list_item_1);
        oAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        xSpinner.setAdapter(xAdapter);
        oSpinner.setAdapter(oAdapter);
        return true;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        return;
    }

    @Override
    /**
     * Calls the method to change `X' or `O' colors depending on which was selected.
     */
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (position) {
            case 0:
                changeTextColor(parent, Color.BLACK);
                break;
            case 1:
                changeTextColor(parent, Color.RED);
                break;
            case 2:
                changeTextColor(parent, Color.GREEN);
                break;
            case 3:
                changeTextColor(parent, Color.BLUE);
                break;
            case 4:
                changeTextColor(parent, Color.CYAN);
                break;
            case 5:
                changeTextColor(parent, Color.MAGENTA);
                break;
            case 6:
                changeTextColor(parent, Color.YELLOW);
                break;
        }
    }

    /**
     * Changes the color of the `X' or `O'
     *
     * @param spinner which spinner the color was selected on
     * @param color   what color to change to
     */
    public void changeTextColor(AdapterView spinner, int color) {
        if (spinner.getId() == R.id.xColor_spinner) // if `X' was selected
            xColor = color;
        else                                       // else change `O'
            oColor = color;
        if (numPlays > 0) { // if game is in progress go back and change color of previous moves made
            if (spinner.getId() == R.id.xColor_spinner)
                retroactiveColorChange(1);
            else
                retroactiveColorChange(4);
        }
    }

    /**
     * Goes through the gameboard and changes the color of `X' or `O'
     *
     * @param player 1 is `X', 4 is `O'
     */
    public void retroactiveColorChange(int player) {
        int count = 0; // how many times a color was changed
        int rounds = (numPlays / 2); // how many times to look for buttons to change the color of
        int color;
        if (player == 1) { // sets the color to the new default color changed in the previous method
            color = xColor;
            rounds++;       // Can only ever be one more `X' then an `O' since `X' always goes first
        } else
            color = oColor;

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (count >= rounds)  // Escape reColoring early if all have been hit
                    return;
                if (gameBoard[i][j] == player) {
                    btnBoard[i][j].setTextColor(color);
                    count++;
                }
            }
        }
    }

    @Override
    /**
     * Listener to the game board buttons
     */
    public void onClick(View v) {

        int id = v.getId();

        switch (id) {
            // which ever button is clicked will pass its self to the "play" method
            case R.id.btn00:
                play(v, 0, 0);
                break;
            case R.id.btn01:
                play(v, 0, 1);
                break;
            case R.id.btn02:
                play(v, 0, 2);
                break;
            case R.id.btn10:
                play(v, 1, 0);
                break;
            case R.id.btn11:
                play(v, 1, 1);
                break;
            case R.id.btn12:
                play(v, 1, 2);
                break;
            case R.id.btn20:
                play(v, 2, 0);
                break;
            case R.id.btn21:
                play(v, 2, 1);
                break;
            case R.id.btn22:
                play(v, 2, 2);
                break;
        }
    }

    /**
     * This method is the entry vector for all gameplay functions
     *
     * @param v the button that was clicked
     * @param r which row the button was clicked in
     * @param c which column the button was clicked in
     */
    public void play(View v, int r, int c) {

        Button b = (Button) v;  // Casts the Vew to a Button so the setText method is accessible.
        b.setClickable(false); // the button should not be able to be clicked again while the game is in progress
        if (xORo) {
            gameBoard[r][c] = 4;  // `O'
            b.setTextColor(oColor);
            b.setText(R.string.oPlay);
            gameStatus.setText(playerXnamePlural + " turn");
            xORo = false;
        } else {
            gameBoard[r][c] = 1;  // `X'
            b.setTextColor(xColor);
            b.setText(R.string.xPlay);
            gameStatus.setText(playerOnamePlural + " turn");
            xORo = true;
        }
        numPlays++;
        if (numPlays >= 5)
            checkBoard(); // only possible to win in five plays min.
    }

    /**
     * looks for a winner
     */
    public void checkBoard() {
        int sum = 0; // total value of the three spaces being checked
        // check rows
        sum = gameBoard[0][0] + gameBoard[0][1] + gameBoard[0][2];
        if (sum > 2) {
            if (sum == 3) {
                gameOver(1, 0, -1);
                return;
            }
            if (sum == 12) {
                gameOver(4, 0, -1);
                return;
            }
        }
        sum = gameBoard[1][0] + gameBoard[1][1] + gameBoard[1][2];
        if (sum > 2) {
            if (sum == 3) {
                gameOver(1, 1, -1);
                return;
            }
            if (sum == 12) {
                gameOver(4, 1, -1);
                return;
            }
        }
        sum = gameBoard[2][0] + gameBoard[2][1] + gameBoard[2][2];
        if (sum > 2) {
            if (sum == 3) {
                gameOver(1, 2, -1);
                return;
            }
            if (sum == 12) {
                gameOver(4, 2, -1);
                return;
            }
        }
        // check columns
        sum = gameBoard[0][0] + gameBoard[1][0] + gameBoard[2][0];
        if (sum > 2) {
            if (sum == 3) {
                gameOver(1, -1, 0);
                return;
            }
            if (sum == 12) {
                gameOver(4, -1, 0);
                return;
            }
        }
        sum = gameBoard[0][1] + gameBoard[1][1] + gameBoard[2][1];
        if (sum > 2) {
            if (sum == 3) {
                gameOver(1, -1, 1);
                return;
            }
            if (sum == 12) {
                gameOver(4, -1, 1);
                return;
            }
        }
        sum = gameBoard[0][2] + gameBoard[1][2] + gameBoard[2][2];
        if (sum > 2) {
            if (sum == 3) {
                gameOver(1, -1, 2);
                return;
            }
            if (sum == 12) {
                gameOver(4, -1, 2);
                return;
            }
        }
        // check diag
        sum = gameBoard[0][0] + gameBoard[1][1] + gameBoard[2][2];
        if (sum > 2) {
            if (sum == 3) {
                gameOver(1, 0, -2);
                return;
            }
            if (sum == 12) {
                gameOver(4, 0, -2);
                return;
            }
        }
        sum = gameBoard[0][2] + gameBoard[1][1] + gameBoard[2][0];
        if (sum > 2) {
            if (sum == 3) {
                gameOver(1, -2, 0);
                return;
            }
            if (sum == 12) {
                gameOver(4, -2, 0);
                return;
            }
        }
        if (numPlays == 9) // case for no winner
            gameOver(0, 0, 0);
        return;
    }

    /**
     * Stops the game and announces the winner in a toast
     *
     * @param winner 1 is `X' 4 is `O'
     * @param r      winning row
     * @param c      winning column
     */
    public void gameOver(int winner, int r, int c) {

        for (int i = 0; i < 3; i++) { // make all buttons unclickable
            for (int j = 0; j < 3; j++) {
                btnBoard[i][j].setClickable(false);
            }
        }

        if (winner != 0) {
            int winColor = colorPrimary; // Color the winning three spaces
            if (c == -1) {
                for (int j = 0; j < 3; j++) {
                    btnBoard[r][j].getBackground().setColorFilter(winColor, PorterDuff.Mode.MULTIPLY);
                }
            } else if (r == -1) {
                for (int i = 0; i < 3; i++) {
                    btnBoard[i][c].getBackground().setColorFilter(winColor, PorterDuff.Mode.MULTIPLY);
                }
            } else if (c == -2) {
                btnBoard[0][0].getBackground().setColorFilter(winColor, PorterDuff.Mode.MULTIPLY);
                btnBoard[1][1].getBackground().setColorFilter(winColor, PorterDuff.Mode.MULTIPLY);
                btnBoard[2][2].getBackground().setColorFilter(winColor, PorterDuff.Mode.MULTIPLY);
            } else if (r == -2) {
                btnBoard[0][2].getBackground().setColorFilter(winColor, PorterDuff.Mode.MULTIPLY);
                btnBoard[1][1].getBackground().setColorFilter(winColor, PorterDuff.Mode.MULTIPLY);
                btnBoard[2][0].getBackground().setColorFilter(winColor, PorterDuff.Mode.MULTIPLY);
            }
        }
        switch (winner) { // announce the winner
            case 1:
                gameStatus.setText(playerXname + " has won the game!");
//                Toast.makeText(getApplicationContext(), playerXname + " has won the game!", Toast.LENGTH_LONG).show();
                break;
            case 4:
                gameStatus.setText(playerOname + " has won the game!");
//                Toast.makeText(getApplicationContext(), playerOname + " has won the game!", Toast.LENGTH_LONG).show();
                break;
            case 0:
                gameStatus.setText("Cat's game, no winner.");
//                Toast.makeText(getApplicationContext(), "Cat's game, no winner.", Toast.LENGTH_LONG).show();
                break;
        }
    }
}
